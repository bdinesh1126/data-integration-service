/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package pipes;

import static com.google.common.base.Preconditions.checkState;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.dataintegrationservice.DataIntegrationServiceApplication;
import ch.post.it.evoting.dataintegrationservice.common.PropertyProvider;
import ch.post.it.evoting.dataintegrationservice.common.StringUtil;
import ch.post.it.evoting.dataintegrationservice.common.XmlTransformer;
import ch.post.it.evoting.dataintegrationservice.domain.RegisterTypeWithVoterStream;
import ch.post.it.evoting.dataintegrationservice.input.DirectoryScanner;
import ch.post.it.evoting.evotinglibraries.xml.XsdConstants;
import ch.post.it.evoting.evotinglibraries.xml.hashable.ConfigurationSignerListener;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;

import li.chee.reactive.plumber.Plumbing;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

public abstract class Tools extends Plumbing {

	private static final Logger LOGGER = LoggerFactory.getLogger(Tools.class);

	public static Predicate<String> fileType(final String type) {
		return (String s) -> DirectoryScanner.filterType(type).test(s);
	}

	public static Consumer<Configuration> saveConfigXML(final String suffixName) {
		final URL xsd = Tools.class.getResource(XsdConstants.CANTON_CONFIG_XSD);
		checkState(xsd != null, "Unable to load XSD file to validate config");
		return o -> saveXML(Configuration.class, null, xsd, String.format("%s.xml", StringUtil.concat("configuration", suffixName, "-"))).accept(o);
	}

	public static Consumer<RegisterTypeWithVoterStream> saveInvalidRegisterXML() {
		return o -> {
			LOGGER.warn("Invalid voters have been generated!");
			saveXML(RegisterTypeWithVoterStream.class, new QName("register"), null, "invalidVoters.xml").accept(o);
		};
	}

	public static <V> Consumer<V> saveXML(final Class<V> jaxbFactoryClass, final QName rootElementName, final URL xsdSchema, final String filename) {
		return o -> {
			try (final OutputStream fos = new FileOutputStream(
					Paths.get(PropertyProvider.getPropertyValue("directory.target", "target"), filename).toFile())) {

				Marshaller.Listener listener = null;
				if (jaxbFactoryClass.equals(Configuration.class)) {
					listener = new ConfigurationSignerListener((Configuration) o,
							DataIntegrationServiceApplication.getContext().getBean(SignatureKeystore.class));
				}

				XmlTransformer.toXml(fos, jaxbFactoryClass, xsdSchema, o, rootElementName, listener);
			} catch (final Exception e) {
				throw new IllegalStateException(e);
			}
		};
	}

	public static <T> Function<T, Integer> batches(final int size) {
		final AtomicInteger counter = new AtomicInteger();
		return (T t) -> counter.getAndIncrement() / size;
	}

	public static <T> Function<T, Mono<Boolean>> hasElements(final Flux<?> flux) {
		return t -> flux.hasElements();
	}

	public static Consumer<Throwable> die(final String message) {
		return error -> {
			LOGGER.error("Error : {}", message, error);
			throw new IllegalStateException(message, error);
		};
	}

	public static <T> Function<Flux<T>, Flux<T>> logAsInfo(final String message) {
		LOGGER.info(message);
		return flux -> flux;
	}

	public static <T> Function<Flux<T>, Flux<T>> errorIfEmpty(final String message) {
		return flux -> flux.switchIfEmpty((Publisher<? extends T>) empty().doOnComplete(() -> {
			throw new IllegalStateException(message);
		}));
	}

	public static Consumer<Object> logItemAsInfo(final String message) {
		return o -> LOGGER.info(message, o);
	}

	public static <T> BiFunction<T, List<Function<T, T>>, T> apply() {
		return (item, plugins) -> plugins.stream().reduce(Function.identity(), Function::andThen).apply(item);
	}

	public static <T, U> BiFunction<Tuple2<T, U>, List<BiFunction<T, U, T>>, T> applyTuple() {
		return (item, plugins) -> plugins.stream().reduce(
				(t, u) -> t,
				(f1, f2) -> (t, u) -> f1.apply(f2.apply(t, u), u)).apply(item.getT1(), item.getT2());
	}

	public static <T, U> Function<Tuple2<T, U>, T> unzip() {
		return Tuple2::getT1;
	}
}